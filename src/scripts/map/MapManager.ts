import "phaser"
import { MAP_INFO, unit } from "../Globals"
import GameScene from "../scenes/GameScene"

export default class MapManager {
  currentMapObject: any
  currentMap: Phaser.Tilemaps.Tilemap
  tiles: Phaser.Tilemaps.Tileset

  floor: Phaser.Tilemaps.StaticTilemapLayer
  walls: Phaser.Tilemaps.StaticTilemapLayer

  constructor() {}

  // Creates map on given scene. If there is no current map object, the default map is created.
  makeMap(scene: Phaser.Scene) {
    this.currentMap = scene.make.tilemap({ key: this.currentMapObject.key })
  }

  addTilesetImage() {
    this.tiles = this.currentMap.addTilesetImage(this.currentMap.tilesets[0].name, this.currentMapObject.tilesKey)
  }

  createLayers() {
    this.floor = this.currentMap.createStaticLayer("floor", this.tiles)
    this.walls = this.currentMap.createStaticLayer("walls", this.tiles)
  }

  getObjectsByNameAndProp(layer: string, property: string) {
    let objectLayer = this.currentMap.getObjectLayer(layer)
    let objects = objectLayer.objects.filter((obj) => obj.type == property)
    return objects
  }

  isThereWall(x: number, y: number) {
    if (this.currentMap.getTileAtWorldXY(x, y, true, undefined, this.walls).properties.collides == true) return true
    else return false
  }

  getLayerByName(name: string) {
    const objLayers = this.currentMap.objects
    const spawnLayer = objLayers.find((el) => el.name == name)
    return spawnLayer
  }

  getSpawnOrigin() {
    const spawnLayer = this.getLayerByName("spawn")
    const spawnOrigin = spawnLayer?.objects.find((el) => el.name == "spawn-origin")!
    return spawnOrigin
  }

  getSpawnTileByKey(teleportFrom: string) {
    const spawnLayer = this.getLayerByName("spawn")
    const TileToTeleportTo = spawnLayer?.objects.find((el) => el.properties[0].value == teleportFrom)
    return TileToTeleportTo
  }

  /**
   * Gives the teleport tile on specified coordinates. If none exists, returns undefined.
   * @param x x coordinate of requested teleport check
   * @param y y coordinate of requested teleport check
   */
  getTeleportAt(x: number, y: number) {
    const tpLayer = this.getLayerByName("teleport")
    const teleportTile = tpLayer?.objects.find((el) => {
      return el.x == x && el.y == y
    })
    return teleportTile
  }

  /**
   *
   * @param propName Name of property of the current map
   */
  getMapPropByName(propName: string) {
    const propsArray: any = this.currentMap.properties
    if (propsArray != undefined) {
      return propsArray.find((prop) => prop.name == propName)
    }
  }

  /**
   * Gives position of an empty place on walls layer.
   * Recursively searches for random place from X:[0, width] Y:[0, height]
   */
  getRandomFreePosition() {
    const randomX = Math.floor(Math.random() * this.currentMap.width) * unit
    const randomY = Math.floor(Math.random() * this.currentMap.height) * unit
    if (this.isThereWall(randomX, randomY)) {
      // console.log("Picked wall: " + randomX / unit + " / " + randomY / unit)
      return this.getRandomFreePosition()
    } else {
      return { x: randomX, y: randomY }
    }
  }

  /**
   * Teleports player to a new map.
   * @param scene Scene object a teleport occurs on. Current map key is used in getting the new spawn position.
   * @param teleportTile Tile which tells where to teleport to. It should have a tp property, whose value is key of the next map.
   */
  teleportFromTile(scene: GameScene, teleportTile: Phaser.Types.Tilemaps.TiledObject) {
    // console.log("Current map: " + this.currentMapObject.key)
    const newMapKey = teleportTile.properties[0].value
    const oldMapKey = this.currentMapObject.key
    // destroy current map
    this.currentMap.destroy()

    // destroy all enemies
    scene.entityManager.clearAllEnemies()

    // select new global map object
    this.currentMapObject = scene.cache.json.get("map-data").find((el) => {
      // this.currentMapObject = MAP_INFO.find((el) => {
      return el.key == newMapKey
    })

    scene.createMap()

    let spawnTile: any //Phaser.Types.Tilemaps.TiledObject
    spawnTile = this.getSpawnTileByKey(oldMapKey)

    scene.entityManager.createPlayer(scene, spawnTile)
    scene.createEntities()
    scene.entityManager.addPlayerToScene(scene)

    scene.createCamera()
  }

  // loading
  loadMaps(scene: Phaser.Scene) {
    // scene.load.image(MAP_INFO[0].tilesKey, MAP_INFO[0].tilesUrl)
    // scene.load.image(MAP_INFO[1].tilesKey, MAP_INFO[1].tilesUrl)
  }

  loadTilesets(scene: Phaser.Scene) {
    scene.load.tilemapTiledJSON(MAP_INFO[0].key, MAP_INFO[0].url)
    scene.load.tilemapTiledJSON(MAP_INFO[1].key, MAP_INFO[1].url)
  }
}

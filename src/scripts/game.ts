import "phaser"

import LoadScene from "./scenes/LoadScene"

const DEFAULT_WIDTH = 800
const DEFAULT_HEIGHT = 480

export default class InitialScene extends Phaser.Scene {
  constructor() {
    super("InitialScene")
  }

  preload() {
    console.log("Starting game")
  }

  create() {
    this.scene.add("load-scene", LoadScene, false)
    this.scene.start("load-scene")
  }
}

const config = {
  type: Phaser.AUTO,
  backgroundColor: 0x666666,
  width: 800,
  height: 480,
  pixelArt: true,
  scale: {
    parent: "root",
    // mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    // width: "100%",
    // height: "100%",
  },
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0, debug: true },
    },
  },
  scene: InitialScene,
}

const game = new Phaser.Game(config)

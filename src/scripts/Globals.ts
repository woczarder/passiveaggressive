export const SCENE_KEYS = {
  LoadScene: "load-scene",
  MenuScene: "menu-scene",
  GameScene: "game-scene",
}
export const unit = 16

export const MAP_INFO = [
  {
    key: "first-city",
    url: "src/assets/maps/tilemaps/first_city.json",
    tilesKey: "first-city-tiles",
    tilesUrl: "src/assets/maps/tilesets/catastrophi_tiles_16.png",
  },
  {
    key: "green-road",
    url: "src/assets/maps/tilemaps/green_road.json",
    tilesKey: "green-road-tiles",
    tilesUrl: "src/assets/maps/tilesets/catastrophi_tiles_16.png",
  },
]
import "phaser"
import Entity from "../Entity"

export default class NPC extends Entity {
  constructor(scene: Phaser.Scene, x: number, y: number, texture: string) {
    super(scene, x, y, texture, "npc")
  }
}

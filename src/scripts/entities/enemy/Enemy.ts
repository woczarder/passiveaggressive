import "phaser"
import Entity from "../Entity"
import { random } from "lodash"
import { unit } from "../../Globals"

export default class Enemy extends Entity {
  target: Entity

  constructor(scene: Phaser.Scene, x: number, y: number, texture: string) {
    super(scene, x, y, texture, "enemy")
  }

  moveRandomly() {
    if (this.active == false) {
      return
    }

    let nextX = 0
    let nextY = 0

    const choice = random(1, 5)
    if (choice == 1) {
      // move right
      nextX = this.x + unit
      nextY = this.y
    } else if (choice == 2) {
      // move left
      nextX = this.x - unit
      nextY = this.y
    } else if (choice == 3) {
      // move up
      nextX = this.x
      nextY = this.y - unit
    } else if (choice == 4) {
      // move down
      nextX = this.x
      nextY = this.y + unit
    } else {
      // don't move
    }

    // if there is wall
    let entScene: any = this.scene
    if (entScene.mapManager.isThereWall(nextX, nextY)) {
      // don't move
    } else {
      // no wall there

      // if there is entity
      const nextEntity = entScene.entityManager.getEntityAt(nextX, nextY)
      if (nextEntity) {
        // Entity present

        // if next Entity is a target
        if (nextEntity == this.target) {
          // next Entity is a target
          // attack
          this.hit(nextEntity)
        } else {
          // next Entity is not a target

          if (nextEntity.type == "loot") {
            // next Entity is Loot
            // move
            this.x = nextX
            this.y = nextY
          } else {
            // next Entity is not Loot
            // don't move
          }
        }
      } else {
        // if there is teleport
        const teleportTile = entScene.mapManager.getTeleportAt(nextX, nextY)
        if (teleportTile != undefined) {
          // there is teleport
          // don't move
        } else {
          // no teleport
          // move
          this.x = nextX
          this.y = nextY
        }
      }
    }
  }
}

import "phaser"
import Entity from "../Entity"

export default class Player extends Entity {
  constructor(scene: Phaser.Scene, x: number, y: number, texture: string) {
    super(scene, x, y, texture, "player")
  }
}

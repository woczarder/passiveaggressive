import "phaser"
import Entity from "../Entity"

export default class Door extends Entity {
  keys: string[]
  shade: Phaser.GameObjects.Rectangle

  constructor(scene: Phaser.Scene, x: number | undefined, y: number | undefined, texture = "door-texture") {
    // clears: map object // object to destroy, when this is destroyed
    super(scene, x, y, texture, "door")
  }

  addShade(scene: Phaser.Scene, shade: Phaser.Types.Tilemaps.TiledObject) {
    // console.log("shade")
    // console.log(shade)

    this.shade = new Phaser.GameObjects.Rectangle(scene, shade.x!, shade.y!, shade.width, shade.height, 0x000000, 1)
    this.shade.setDisplayOrigin(0, 0)
    scene.add.existing(this.shade)
  }

  destroyDoor() {
    this.destroyShade()
    this.destroy()
  }

  destroyShade() {
    if (this.shade) this.shade.destroy()
  }
  lockWith() {}
}

import "phaser"
import {} from "../Globals"

export default class Entity extends Phaser.GameObjects.Sprite {
  readonly type: string

  constructor(scene: Phaser.Scene, x = 16, y = 16, texture: string, type: string) {
    super(scene, x, y, texture)
    this.type = type
    this.setOrigin(0)
    this.setDisplayOrigin(0)
  }

  hit(target: Entity) {
    target.destroy()
  }
}

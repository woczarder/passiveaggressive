import "phaser"
import Entity from "../Entity"

export default class Loot extends Entity {
  constructor(scene: Phaser.Scene, x: number, y: number, texture: string) {
    super(scene, x, y, texture, "loot")
  }
}

import "phaser"
import { MAP_INFO, unit } from "../Globals"
import Player from "./player/Player"
import Enemy from "./enemy/Enemy"
import Door from "./door/Door"
import Entity from "./Entity"

export default class EntityManager {
  player: Player
  enemies: Array<Enemy>
  doors: Array<Door>

  constructor() {
    this.enemies = []
    this.doors = []
  }

  loadImages(scene: Phaser.Scene) {
    // scene.load.image("player-texture", "src/assets/sprites/player.png")
    // scene.load.image("enemy-texture", "src/assets/sprites/enemy.png")
    // scene.load.image("doors-texture", "src/assets/sprites/doors.png")
  }

  createPlayer(scene: Phaser.Scene, spawnTile: Phaser.Types.Tilemaps.TiledObject) {
    if (spawnTile == undefined) {
      this.player = new Player(scene, 16, 16, "player-texture")
    } else {
      this.player = new Player(scene, spawnTile.x!, spawnTile.y!, "player-texture")
    }
  }

  addPlayerToScene(scene: Phaser.Scene) {
    scene.add.existing(this.player)
  }

  addEnemy(scene: Phaser.Scene, x: number, y: number) {
    let data = scene.cache.json.get("enemy-data")
    // console.log(data)
    let enemy = new Enemy(scene, x, y, "enemy-texture")
    this.enemies.push(enemy)
    scene.add.existing(enemy)
  }

  addDoor(
    scene: Phaser.Scene,
    door: Phaser.Types.Tilemaps.TiledObject,
    shade: Phaser.Types.Tilemaps.TiledObject | undefined,
    keys: Phaser.Types.Tilemaps.TiledObject | undefined
  ) {
    // console.log("door")
    // console.log(door)
    let doorTexture = door.properties.find((prop: any) => prop.name == "texture")

    let newDoor = new Door(scene, door.x, door.y, doorTexture.value)

    // if there is shade, add it to scene
    if (shade) {
      newDoor.addShade(scene, shade)
    }

    // if there are keys, add them to door
    scene.add.existing(newDoor)
    this.doors.push(newDoor)
  }

  getEnemyAt(x: number, y: number) {
    const searchedEnemy = this.enemies.find((enemy) => enemy.x == x && enemy.y == y)
    return searchedEnemy
  }
  getDoorAt(x: number, y: number) {
    const searchedDoor = this.doors.find((enemy) => enemy.x == x && enemy.y == y)
    return searchedDoor
  }

  getEntityAt(x: number, y: number) {
    // enemies
    let searchedEntity: Entity | undefined = this.getEnemyAt(x, y)
    if (searchedEntity != undefined) {
      return searchedEntity
    }

    // doors
    searchedEntity = this.getDoorAt(x, y)
    if (searchedEntity != undefined) {
      return searchedEntity
    }

    if (this.player.x == x && this.player.y == y) {
      return this.player
    }
  }

  moveEnemies() {
    this.enemies.forEach((enemy) => {
      enemy.moveRandomly()
    })
  }

  cleanUpDeadEntities() {
    this.enemies = this.enemies.filter((enemy) => enemy.active == true)

    // doors
    for (let i = this.doors.length - 1; i >= 0; i--) {
      if (!this.doors[i].active) {
        this.doors[i].destroyDoor()
        this.doors.splice(i, 1)
      }
    }
  }

  clearAllEnemies() {
    this.enemies.forEach((enemy) => {
      enemy.destroy()
    })
    this.enemies = []
  }
}

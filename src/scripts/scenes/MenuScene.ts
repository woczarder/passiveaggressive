import "phaser"

import { SCENE_KEYS } from "../Globals"
import GameScene from "./GameScene"

export default class MenuScene extends Phaser.Scene {
  constructor() {
    super({ key: SCENE_KEYS.MenuScene })
  }

  init() {
    console.log("[01] MenuScene init")
  }

  preload() {
    console.log("[02] MenuScene preload")
  }

  create() {
    console.log("[03] MenuScene create")
    this.add.text(100, 100, "You are in menu", {
      font: "16px Arial",
      fill: "#FFFFFF",
    })
    this.add.text(100, 200, "Press space to start game, retard", {
      font: "16px Arial",
      fill: "#FFFFFF",
    })

    this.scene.add(SCENE_KEYS.GameScene, GameScene, false)
    this.scene.start(SCENE_KEYS.GameScene, GameScene)
  }
}

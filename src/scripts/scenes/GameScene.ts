import "phaser"

import MapManager from "../map/MapManager"
import EntityManager from "../entities/EntityManager"

import { SCENE_KEYS, unit, MAP_INFO } from "../Globals"
import InputHandler from "../input/InputHandler"

export default class GameScene extends Phaser.Scene {
  mapManager: MapManager
  entityManager: EntityManager
  inputHandler: InputHandler

  constructor() {
    super({ key: SCENE_KEYS.GameScene })
    this.mapManager = new MapManager()
    this.entityManager = new EntityManager()
    this.inputHandler = new InputHandler()
  }

  init() {
    console.log("[01] GameScene init")
  }

  preload() {
    console.log("[02] GameScene preload")

    // maps
    // this.mapManager.loadMaps(this) // loaded in pack now

    // tilemaps
    this.mapManager.loadTilesets(this)

    // entities
    this.entityManager.loadImages(this) // loaded in pack now

    // entity data
    this.load.json("enemy-data", "src/assets/enemies.json")
    let data = this.cache.json.get("enemy-data")

    this.load.json("map-data", "src/assets/maps.json")

    this.load.pack("pack", "src/assets/mapscopy.json", "maps.files")
  }

  create() {
    console.log("[03] GameScene create")

    console.log(this.cache.json.get("pack"))
    console.log(this.cache.tilemap.get("first-city1"))
    // console.log(this.cache.image.get("first-city-tiles"))

    // Input
    this.input.keyboard.on("keydown", this.onKeyInput, this)

    this.mapManager.currentMapObject = this.cache.json.get("map-data")[0]
    this.createMap()
    this.createPlayer()
    this.createEntities()
    this.createCamera()
  }

  createMap() {
    this.mapManager.makeMap(this)
    this.mapManager.addTilesetImage()
    this.mapManager.createLayers()
  }

  createPlayer() {
    const spawn = this.mapManager.getSpawnOrigin()
    this.entityManager.createPlayer(this, spawn)
    this.entityManager.addPlayerToScene(this)
  }

  createEntities() {
    let enemiesCount = this.mapManager.getMapPropByName("enemies-count").value
    let i = 0
    while (i < enemiesCount) {
      let freeTile = this.mapManager.getRandomFreePosition()
      if (
        this.entityManager.getEnemyAt(freeTile.x, freeTile.y) == undefined &&
        this.entityManager.player.x != freeTile.x &&
        this.entityManager.player.y != freeTile.y
      ) {
        this.entityManager.addEnemy(this, freeTile.x, freeTile.y)
        i++
      }
    }

    let doorsArray = this.mapManager.getObjectsByNameAndProp("doors", "door")
    let shadesArray = this.mapManager.getObjectsByNameAndProp("doors", "shade")
    let shade: Phaser.Types.Tilemaps.TiledObject | undefined
    // console.log(shadesArray)
    let keys: any
    doorsArray.forEach((door) => {
      shade = undefined
      // console.log("door")
      // console.log(door)

      let doorShadeProp = door.properties.find((prop) => prop.name == "shade") // shade prop on door

      if (doorShadeProp != undefined) {
        // console.log("doorShadeProp")
        // console.log(doorShadeProp)

        shade = shadesArray.find((el) => el.id == doorShadeProp.value)
        // console.log("shade")
        // console.log(shade)
      }

      keys = door.properties.find((prop) => prop.name == "key")

      this.entityManager.addDoor(this, door, shade, keys)
    })

    // // add 1 door
    // let door = new Door(this, 32, 64, "doors-texture")
    // this.add.existing(door)
  }

  createCamera() {
    this.cameras.main.setBounds(
      0,
      0,
      this.mapManager.currentMap.widthInPixels,
      this.mapManager.currentMap.heightInPixels
    )
    this.cameras.main.startFollow(this.entityManager.player)
    this.cameras.main.setZoom(2)
  }

  onKeyInput(event: any) {
    this.inputHandler.handleInput(event, this)
  }
}

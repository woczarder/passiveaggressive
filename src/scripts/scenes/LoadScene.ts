import "phaser"

import { SCENE_KEYS } from "../Globals"
import MenuScene from "./MenuScene"

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({ key: SCENE_KEYS.LoadScene })
  }

  init() {
    console.log("[01] LoadScene init")
  }

  preload() {
    console.log("[02] LoadScene preload")
  }

  create() {
    console.log("[03] LoadScene create")
    this.add.text(100, 100, "You are in loading shit scene", {
      font: "16px Arial",
      fill: "#FFFFFF",
    })
    this.scene.add(SCENE_KEYS.MenuScene, MenuScene, false)
    this.scene.start(SCENE_KEYS.MenuScene, MenuScene)
  }
}

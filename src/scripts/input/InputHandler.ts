import "phaser"

import GameScene from "../scenes/GameScene"
import { SCENE_KEYS, unit, MAP_INFO } from "../Globals"

export default class InputHandler {
  constructor() {}

  handleInput(event: any, scene: GameScene) {
    let nextX = 0
    let nextY = 0

    if (event.code === "ArrowUp") {
      console.log("UP")
      nextX = scene.entityManager.player.x
      nextY = scene.entityManager.player.y - unit
    } else if (event.code === "ArrowDown") {
      console.log("DOWN")
      nextX = scene.entityManager.player.x
      nextY = scene.entityManager.player.y + unit
    } else if (event.code === "ArrowLeft") {
      console.log("LEFT")
      nextX = scene.entityManager.player.x - unit
      nextY = scene.entityManager.player.y
    } else if (event.code === "ArrowRight") {
      console.log("RIGHT")
      nextX = scene.entityManager.player.x + unit
      nextY = scene.entityManager.player.y
    } else {
      // nextX = scene.entityManager.player.x
      // nextY = scene.entityManager.player.y
    }

    // const x = ""
    // console.log(x)

    // if there is wall
    if (scene.mapManager.isThereWall(nextX, nextY)) {
      // don't move
    } else {
      // no wall there

      // if there is entity
      const nextEntity = scene.entityManager.getEntityAt(nextX, nextY)
      if (nextEntity) {
        // Entity present

        // is it NPC
        if (nextEntity.type == "npc") {
          // interact
        }
        // is it Enemy
        else if (nextEntity.type == "enemy") {
          // fight
          scene.entityManager.player.hit(nextEntity)
          scene.entityManager.cleanUpDeadEntities()
        }
        // is it Loot
        else if (nextEntity.type == "loot") {
          // move
          // pick up
        }
        // is it Door
        else if (nextEntity.type == "door") {
          console.log("DOOOOOR")
          // do you have a key
          if (true) {
            // have correct key
            // destroy doors
            nextEntity.active = false
          } else {
            // no key
            // don't move
            // show popup
          }
        } else {
          // don't move
        }
      } else {
        // no Entity there

        // if there is teleport
        const teleportTile = scene.mapManager.getTeleportAt(nextX, nextY)
        if (teleportTile != undefined) {
          // there is teleport
          // teleport
          scene.mapManager.teleportFromTile(scene, teleportTile)
        } else {
          // no teleport
          // move
          scene.entityManager.player.x = nextX
          scene.entityManager.player.y = nextY
        }
      }
    }
    // cleanup dead bodies
    scene.entityManager.cleanUpDeadEntities()
    // move enemies and stuff
    scene.entityManager.moveEnemies()
    // scene.entityManager.moveEnemies()
  }
}
